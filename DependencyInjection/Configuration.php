<?php

namespace Devy\WeatlasAPIBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('devy_weatlas_api');
        
        $rootNode
            ->children()
                ->scalarNode('aid')
                    ->defaultValue('14039')
                ->end()
                ->scalarNode('key')
                    ->defaultValue('7e6cdf6e21bf874fd304826d2bf63527')
                ->end()
            ->end();

        return $treeBuilder;
    }
}
