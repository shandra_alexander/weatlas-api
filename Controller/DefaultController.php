<?php

namespace Devy\WeatlasAPIBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Devy\WeatlasAPIBundle\Service\WeatlasAPI;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{

    public function searchAction(Request $request)
    {
        $term = $request->get('term');
        /* @var $api WeatlasAPI */
        $api = $this->get('devy_weatlas_api');
        $result = $api->search($term);
        return new JsonResponse($result);
    }

}
